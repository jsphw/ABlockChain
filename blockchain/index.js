const Block = require("./block");

class Blockchain {
  constructor() {
    this.chain = [Block.genesis()];
  }

  addBlock(data) {
    const lastBlock = this.chain[this.chain.length - 1];
    const block = Block.mineBlock(lastBlock, data);
    this.chain.push(block);
    console.log(this.chain);
    return block;
  }

  isValidChain(chain) {
    if (JSON.stringify(chain[0]) !== JSON.stringify(Block.genesis())) {
      return false;
    }

    for (let index = 1; index < chain.length; index++) {
      const block = chain[index];
      const lastBlock = chain[index - 1];

      if (
        block.lastHash !== lastBlock.hash ||
        block.hash !== Block.blockHash(block)
      ) {
        return false;
      }

      return true;
    }
  }

  replaceChain(newChain) {
    if (newChain.length <= this.chain.length) {
      console.log("shorter chain!");
      return;
    } else if (!this.isValidChain(newChain)) {
      console.log("not a valid chain");
      console.log(newChain);
      return;
    }
    console.log(
      `replacing chain of ${this.chain.length} with new chain of ${newChain.length}`
    );
    this.chain = newChain;
  }
}

module.exports = Blockchain;
