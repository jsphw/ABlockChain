const ChainUtil = require("../chain-util");
const { DIFFICULTY, MINE_RATE } = require("../config");

class Block {
  constructor(timeStamp, lastHash, hash, data, nonce, difficulty) {
    this.timeStamp = timeStamp;
    this.lastHash = lastHash;
    this.hash = hash;
    this.data = data;
    this.nonce = nonce;
    this.difficulty = difficulty || DIFFICULTY;
  }

  toString() {
    return `Block: {
            timeStamp:  ${this.timeStamp} 
            lastHash:   ${this.lastHash.substring(0, 10)} 
            hash:       ${this.hash.substring(0, 10)} 
            data:       ${this.data}
            nonce:      ${this.nonce}
            difficulty: ${this.difficulty}
        }`;
  }

  static genesis() {
    return new this(
      Date.parse("21 Oct 2021 17:39:00:21 GMT+1"),
      128 * 213,
      Block.hash("none", "none", 128 * 213),
      [],
      0,
      DIFFICULTY
    );
  }

  static mineBlock(lastBlock, data) {
    const lashHash = lastBlock.hash;
    let hash, timestamp;
    let { difficulty } = lastBlock;
    let nonce = 0;
    do {
      nonce++;
      timestamp = Date.now();
      difficulty = Block.adjustDifficulty(lastBlock, timestamp);
      hash = Block.hash(timestamp, lashHash, data, nonce, difficulty);
      console.log(hash, "\n");
    } while (hash.substring(0, difficulty) !== "0".repeat(difficulty)); // does it match the leading 0s of the difficulty?

    return new this(timestamp, lashHash, hash, data, nonce, difficulty);
  }

  static hash(lastStamp, lastHash, data, nonce, difficulty) {
    return ChainUtil.hash(
      `${lastStamp}${lastHash}${data}${nonce}${difficulty}`
    ).toString();
  }

  static blockHash(block) {
    return Block.hash(
      block.timeStamp,
      block.lastHash,
      block.data,
      block.nonce,
      block.difficulty
    );
  }

  static adjustDifficulty(lastBlock, currentTime) {
    let { difficulty } = lastBlock;
    if (lastBlock.timeStamp + MINE_RATE > currentTime) {
      difficulty += 1;
    } else if (lastBlock.timeStamp + MINE_RATE < currentTime) {
      difficulty -= 1;
    }
    return difficulty;
  }
}

module.exports = Block;
