    █████╗ ██████╗  ██████╗
    ██╔══██╗██╔══██╗██╔════╝
    ███████║██████╔╝██║     
    ██╔══██║██╔══██╗██║     
    ██║  ██║██████╔╝╚██████╗
    ╚═╝  ╚═╝╚═════╝  ╚═════╝
                            
# A____sBlockChain

## The block

* timestamp in milliseconds

* lashHash (the hash of the block before it)

* hash (the hash of the current block)

* the data that we want to store

* The **genesis block** is the first block of the block-chain and everything else is derived from this!

## The chain

* the chain refers to the information connecting the blocks

* the hashes and past hashes mean that it is easy to verify if a block is in the right place on the chain

* the chain can be extended by other people submitting blocks and will default to the longest, correct chain

## Peer to peer

* the user who opens the first instance opens up the websockets to allow multiple nodes to come in

* new users connect to the websocket and all the peers will be able to connect and send data

* our server will know whether to connect or to allow connectors

Adding peers:

1st: `npm run dev`

2nd: `HTTP_PORT=3002 P2P_PORT=5002 PEERS=ws://localhost:5001 npm run dev`

3rd: `HTTP_PORT=3003 P2P_PORT=5003 PEERS=ws://localhost:5001,ws://localhost:5002 npm run dev`

etc.

## Proof of work

* any peer has the ability to replace the entire blockchain and so in order to deter requests (or make them harder) we can use a **proof-of-work** system.

* individual nodes have to put in a _little_ bit of work to add a block but in order to modify the entire chain you would have to put in the same amount of work as the number of peers and blocks.

* difficulty is a number that represents the number of leading 0s in a hash which the miner will have to match

```
difficulty = 6

hash = 000000this2021isahash
```

* we can also use Nonces to adjust the value to generate new hashes for the data

* once the new nonce is added then everyone accepts it

* we compare the time between the two most recent blocks and compare it to our MINE_RATE and adjusts it so blocks are added to the chain at a consistent level

* an example of the difficulty working:

```
 Block {
   ...
   // three difficulty ergo three leading 0s
    hash: '000c6ae3c72db2b6efea97ccb591b551ba1720f545fc5e2fb7a994a8b8aaa217',
    ...
    difficulty: 3
  },
  Block {
    ...
    // two difficulty ergo two leading 0s
    hash: '00a1a374947c9b33e5269e776e8a5e329b5c88b6d7a13d868035710db7cb2798',
    ...
    difficulty: 2
  }
```

## Wallets and transactions

* wallet is the core object - it has a balance, a public key and a private key. 

* transactions:

```
┌──────────────────────────────┐
│        transaction           │
│                              │
│──────────────────────────────│
│                              │
│  input: timestamp            │
│  balance: 500, signature     │
│  sender's public key: 0xfoo  │
│──────────────────────────────│
│      (sending to another)    │
│  output:                     │
│  amount: 40                  │
│  address: 0xbar              │
│                              │
├──────────────────────────────┤
│ (sending remainder to self)  │
│  output:                     │
│  amount: 460                 │
│  address: 0xf00              │
│                              │
└──────────────────────────────┘
```
* transactions are verified based on signatures generated from a wallet's public key, private key and recipent address, we then verify this using the `secp256k1` module.

* each transaction can have multiple outputs due to the blockchain only getting updated every 10 mins or so.

# Requests
## Post
`localhost:3001/blocks` - or whichever port you specify
```
{
    "data": "whatever"
}
```
The blockchain will then increment based on data send - regardless of which port it is being sent to. Additionally all other ports will share the updated, longer blockchain.

# Requests
## Get
`localhost:3001/blocks` - or whichever port you specify
Returns list of current blocks

# Next steps

* Extend the functionality of wallets and transactions to allow for NFTs to be pushed onto the blockchain

* Host this online and allow multiple peers to connect and add blocks

* Make this private 

* Extend our backend to create wallets and certificates so the customer is only aware of the ownership and transaction history of the certificate

* Discussion - if this is not public then what's the point of decentralisation? The 51% problem would be more easily overwhelmed? Why would people be incentivised by this without income like `bitcoin` or `litecoin`? A bigger discussion to be had.

## Further reading

[Bitcoin: A Peer-to-Peer Electronic Cash System](https://bitcoin.org/bitcoin.pdf)
