const express = require("express");
const bodyParser = require("body-parser");

const P2pServer = require("./p2p-server");
const Wallet = require("../wallet");
const Miner = require("./miner");
const TransactionPool = require("../wallet/transaction-pool");

const Blockchain = require("../blockchain");
const HTTP_PORT = process.env.HTTP_PORT || 3001;

const app = express();
const blockChain = new Blockchain();
const wallet = new Wallet();
const tp = new TransactionPool();
const p2pServer = new P2pServer(blockChain, tp);
const miner = new Miner(blockChain, tp, wallet, p2pServer);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/public-key", (req, res) => {
  res.json({ publicKey: wallet.publicKey });
});

app.get("/blocks", (req, res) => {
  res.json(blockChain.chain);
});

app.post("/blocks", (req, res) => {
  if (req.body.data === undefined) {
    res.status(400);
    res.json({ message: "bad request" });
    return;
  }
  blockChain.addBlock(req.body.data);
  p2pServer.syncChains();

  res.redirect("/blocks");
});

app.get("/transactions", (req, res) => {
  res.json(tp.transactions);
});

app.get("/mine-transactions", (req, res) => {
  const block = miner.mine();
  console.log(`New block address: ${block.toString()}`);
  res.redirect("/blocks");
});

app.post("/transaction", (req, res) => {
  const { recipient, amount } = req.body;
  const transaction = wallet.createTransaction(
    recipient,
    amount,
    blockChain,
    tp
  );
  p2pServer.broadcastTransaction(transaction);
  res.redirect("/transactions");
});

app.listen(HTTP_PORT);
console.log(`listening on ${HTTP_PORT}`);
p2pServer.listen();
